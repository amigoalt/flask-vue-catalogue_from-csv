from flask import Flask
from app.admin import admin
from app.main import main
from app.main.models import (
    database, Catalogue, Product, Brand, ProductBrand, Price)
from app.admin.models import File
from app.admin.jobs import rq


def create_app():
    app = Flask(__name__)
    app.config.update(
        SECRET_KEY=b'_5#y2L"F4Q8z\n\xec]/'
    )
    app.config.update(
        UPLOAD_FOLDER=app.config.root_path + '/static/files/'
    )
    app.config.update(
        MAX_CONTENT_LENGTH=500000
    )

    register_extensions(app)
    register_blueprints(app)

    ''' It's only needed at first start '''
    init_db(app)

    @app.before_request
    def _db_connect():
        database.connect()

    # This hook ensures that the connection is closed when we've finished
    # processing the request.
    @app.teardown_request
    def _db_close(exc):
        if not database.is_closed():
            database.close()

    return app


def register_extensions(app):
    """Register extensions with the Flask application."""
    rq.init_app(app)


def register_blueprints(app):
    """Register blueprints with the Flask application."""
    app.register_blueprint(main)
    app.register_blueprint(admin, url_prefix='/admin')


def init_db(app):
    # database.drop_tables([Product, Brand, ProductBrand, File])
    # database.drop_tables([File])
    database.create_tables([Price, File])

    # NEXT
    # Catalogue().add_or_update_products(dataFrame)
    # Catalogue().add_or_update_products_brands(dataFrame)

    # Catalogue().add_or_update_item(rows)
