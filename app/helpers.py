import re


def alpha_numeric(str=''):
    '''
    Проверить состоит ли строка только из цифр и букв.
    Необходимо для выявления кода товара, например в наименовании товара
    '''
    # return bool(re.match('^(?=.*[0-9]$)(?=.*[a-zA-Z])', str))
    return bool(re.match('[/.a-zA-Z0-9-]+', str))
