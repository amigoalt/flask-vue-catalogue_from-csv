const example = {
    delimiters: ['[[', ']]'],
    data() {
        return {
            data: [],
            name: document.currentScript.getAttribute('filter'),
            selected: null,
            isFetching: false
        }
    },
    watch: {
        // If search field is cleared by user
        name: function () {
            if (this.name.length === 0) {
                window.location = '/catalogue/';  
            }
        },
        selected: function () {
            if (this.selected !== null) {
                window.location = '/catalogue/search/' + this.selected;
            }
        }
    },
    methods: {
        // You have to install and import _.debounce to use it,
        // it's not mandatory though.
        getAsyncData: function (e) {
            if (e.key === "Enter" && this.name.length > 1) {
                window.location = '/catalogue/search/' + this.name; 
            } else {
                this.isFetching = true
                axios.get(`/api/search/${this.name}`).then(({ data }) => {
                    this.data = []
                    data.results.forEach((product) => this.data.push(product.name))
                }).catch((error) => {
                    this.data = []
                    throw error
                })
                .finally(() => {
                    this.isFetching = false
                })
            }
        }
    }
}
const app = new Vue(example)
app.$mount('#autocomplete_search')



Vue.component('shop-basket', {
    data: function() {
        return {
            ordered_list: []
        }
    },
    watch: {
    },
    computed: {
        products_num: function() {
            var count = this.ordered_list.length
            return count
        }
    },
    created(){
        eventBus.$on("update_ordered_list", (ordered_list)=>{
            this.ordered_list = ordered_list
        });
    },
    mounted() {
        var result = ''
        var ordered_list = document.currentScript.getAttribute('ordered_list')
        console.log('MOUNTED = ', ordered_list)
        if( 'None' !== ordered_list && '[]' !== ordered_list && ordered_list.length > 0 && typeof(JSON.parse(ordered_list)) === 'object') {
            console.log('ordered_list = ', ordered_list)
            this.ordered_list = JSON.parse(ordered_list)
        } 
    },
    methods: {
        
    }
});

new Vue({
    delimiters: ['[[', ']]'],
    el: '#shop_basket'
});



        

document.addEventListener('DOMContentLoaded', () => {

// Get all "navbar-burger" elements
const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

// Check if there are any navbar burgers
if ($navbarBurgers.length > 0) {

    // Add a click event on each of them
    $navbarBurgers.forEach( el => {
        el.addEventListener('click', () => {

            // Get the target from the "data-target" attribute
            const target = el.dataset.target;
            const $target = document.getElementById(target);

            // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
            el.classList.toggle('is-active');
            $target.classList.toggle('is-active');
        });
    });
}
});