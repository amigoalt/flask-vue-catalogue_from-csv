const reset_search = {
    data: function() {
        return {
            checkboxGroup: []
        }
    },
    watch: {
        checkboxGroup: function() {
            app.name = '';
        }
    }
}
const reset_search_app = new Vue(reset_search)
reset_search_app.$mount('#reset_search')

Vue.component('add-order-button', {
    props: {
        id: 0
    },
    data: function() {
        return {
            ordered_list: [],
            checked: false,
            activity: 'Заказать'
        }
    },
    watch: {
        checked: function() {
            if (this.checked) {
                this.activity = 'Уже в заказе'
            } else {
                this.activity = 'Заказать'
            }
        }
    },
    created(){
        eventBus.$on("update_ordered_list", (ordered_list)=>{
            this.ordered_list = ordered_list
        });
        var ordered_list_from_server = document.currentScript.getAttribute('ordered_list')
        
        if( 'None' !== ordered_list_from_server && '[]' !== ordered_list_from_server && ordered_list_from_server.length > 0 && typeof(JSON.parse(ordered_list_from_server)) === 'object') {
            this.ordered_list = JSON.parse(ordered_list_from_server)
            for (var i=0; i<this.ordered_list.length; i++) {
                if (this.id === this.ordered_list[i]) {
                    this.checked = true
                    break
                }
            }
        }
    },
    methods: {
        toggleActivity: function(id) {
            if (this.checked == false) {
                axios.post(`/api/session`, { id: id, 'command': 'add' }).then(({ data }) => {
                    eventBus.$emit("update_ordered_list", data.result);
                }).catch((error) => {
                    throw error
                })
                .finally(() => {
                    this.isFetching = false
                })

            } else if (this.checked == true) {
                axios.post(`/api/session`, { id: id, 'command': 'remove' }).then(({ data }) => {
                    eventBus.$emit("update_ordered_list", data.result);
                }).catch((error) => {
                    throw error
                })
                .finally(() => {
                    this.isFetching = false
                })
            }
            this.checked = !this.checked
        }
    }
});

new Vue({
    delimiters: ['[[', ']]'],
    el: '#product_list'
});