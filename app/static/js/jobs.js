Vue.component('run-populating', {
    data: function() {
        return {
            checked: false,
            run_status: ''
        }
    },
    methods: {
        runPopulating: function() {
            console.log('clicked')
            axios.get(`/api/admin/populate/run`).then(({ data }) => {
                if(data.result === 'started') {
                    eventBus.$emit("update_populating_started")
                    this.checked = true
                    this.run_status = '<p color="green">Процесс запущен.</p>'
                } else {
                    eventBus.$emit("update_populating_error")
                    this.checked = false
                    this.run_status = '<p color="red">Ошибка запуска.</p>'
                }
            }).catch((error) => {
                throw error
            })
            .finally(() => {
                console.log('Finally')
            })
        }
    }
});

new Vue({
    delimiters: ['[[', ']]'],
    el: '#populating'
});