import os
from datetime import datetime
import peewee as pw
from flask import current_app
import pandas as pd
# import datetime
# from math import ceil
from app.database import database
from app.main.models import Product, ProductBrand, Brand
import app.helpers as hlp


class CustomEx(BaseException):
    pass


class BaseModel(pw.Model):
    class Meta:
        database = database


class Task(BaseModel):
    task_id = pw.CharField()
    item_id = pw.ForeignKeyField(Product, backref="tasks")
    name = pw.CharField()
    status = pw.CharField(default='valid')
    status_message = pw.CharField(null=True)
    content = pw.TextField(null=True)

    class meta:
        table_name = 'tests'

    @staticmethod
    def create_tasks_records():
        pass


class File(BaseModel):
    file_name = pw.CharField(unique=True)
    file_path = pw.CharField()
    file_status = pw.CharField(default='new')
    total_rows = pw.IntegerField(null=True)
    created = pw.DateField(default=datetime.now())

    class meta:
        table_name = 'files'

    def upload(self, request):
        try:
            f = request.files['file']
            folder = current_app.config['UPLOAD_FOLDER']
            file_path = os.path.join(folder, f.filename)
            f.save(file_path)
        except Exception as ex:
            ex.description = 'Ошибка загрузки файла: ' + ex.description
            raise ex
        else:
            return f.filename, file_path

    def savedb(self, fname, fpath):
        try:
            self.file_name = fname
            self.file_path = fpath
            self.save()
            return(self)
        except Exception as ex:
            ex.args = ('Ошибка БД: ',) + ex.args
            raise ex


class Manager:
    @staticmethod
    def parse_csv_products(filename=''):
        try:
            folder = current_app.config['UPLOAD_FOLDER']
            file_path = os.path.join(folder, filename)
            df_csv = pd.read_csv(file_path, sep=',')
        except BaseException as ex:
            ex.args += ('Функция: parse_csv_products',)
            raise ex
        return df_csv

    @staticmethod
    def substract_code(product_title):
        splitted_title = product_title.split(' ', 1)
        valid_code = splitted_title[0] if hlp.alpha_numeric(
            splitted_title[0]) is True else None
        return valid_code

    @staticmethod
    def add_or_update_brand(df):
        only_existed = df['brand'].notnull()
        brands = df[only_existed]['brand']
        records = pd.DataFrame({
            'brand_name': brands.unique()}).to_dict(orient='record')
        try:
            database.drop_tables([Brand])
            database.create_tables([Brand])
            with database.atomic():
                for batch in pw.chunked(records, 100):
                    Brand.insert_many(batch).execute()
        except pw.DatabaseError as ex:
            ex.args += ('Функция: add_or_update_brand',)
            raise ex

    @staticmethod
    def add_or_update_products(df):
        only_existed = df['name'].notnull()
        columns = ['price', 'name', 'photo']  # from csv file
        '''
        Добавить колонку code, в которой будем хранить код запчасти
        '''
        df = df[only_existed][columns]
        df['code'] = pd.DataFrame(
            {'code': df['name'].apply(Manager.substract_code)})
        columns.append('code')

        records = df[only_existed][columns].to_dict(orient='record')

        try:
            database.drop_tables([Product])
            database.create_tables([Product])
            with database.atomic():
                for batch in pw.chunked(records, 100):
                    Product.insert_many(batch).execute()
        except pw.DatabaseError as ex:
            ex.args += ('Функция: add_or_update_products',)
            raise ex

    @staticmethod
    def add_or_update_products_brands(df):
        # Get brands from csv column 'brand'
        only_existed = df['brand'].notnull()
        brands = df[only_existed]['brand']
        # Make unique brands list
        uniques = pd.DataFrame({'brand': brands.unique()})
        # 'name' of equipment and 'brand' colums from csv
        products = df[only_existed][['name', 'brand']]

        # make product_brand associacion
        products_brands = pd.DataFrame({
            'brand': products.to_dict(orient='list')['brand'],
            'product_id': products.index})
        result = pd.merge(products_brands, uniques, on='brand', how='right')
        '''
        make brand ids starting from 1 (1,2,3...) instead (0,1,2..)
        because they should be stored in db
        '''
        brand_ids = map((lambda bid: bid + 1), uniques.index)
        brand_ids = list(brand_ids)

        columns = ['product_id', 'brand']
        products_brands = result.replace(uniques.values, brand_ids)[columns]
        products_brands = products_brands.rename(columns={"brand": "brand_id"})
        records = products_brands.to_dict(orient='record')
        try:
            database.drop_tables([ProductBrand])
            database.create_tables([ProductBrand])
            with database.atomic():
                for batch in pw.chunked(records, 100):
                    ProductBrand.insert_many(batch).execute()
        except pw.DatabaseError as ex:
            ex.args += ('Функция: add_or_update_products_brands',)
            raise ex

    @staticmethod
    def populate_db(filename):
        try:
            df_csv = Manager.parse_csv_products(filename)
            Manager.add_or_update_brand(df_csv)
            Manager.add_or_update_products(df_csv)
            Manager.add_or_update_products_brands(df_csv)
        except Exception as ex:
            return {'error': ex.args}
        return {'success': 'ok'}
