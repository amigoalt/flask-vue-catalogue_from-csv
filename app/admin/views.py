import os
from flask import (
    render_template,
    session,
    json,
    request
)
from werkzeug import secure_filename
from app.admin import admin
from .jobs import calculate
from app.admin.models import File, Manager


@admin.route('/')
def files():
    return render_template('admin/files.html')


@admin.route('/products')
def products():
    return render_template('admin/products.html')


@admin.route('/tasks')
def tasks():
    return render_template('admin/tasks.html')


@admin.route('/upload')
def upload():
    return render_template('admin/files.html')


@admin.route('/uploader', methods=['GET', 'POST'])
def uploader():
    msg = ''
    file = File()
    try:
        filename, filepath = file.upload(request)
        file.savedb(filename, filepath)
    except Exception as ex:
        if ex.args:
            msg = ' '.join(ex.args)
        else:
            msg = ex.description
    else:
        msg = 'Файл загружен успешно'
    finally:
        return render_template('admin/files.html', msg=msg)


@admin.route('/parse/csv/<filename>')
def parse_csv(filename):
    status = Manager.populate_db(filename)
    return json.dumps(status, ensure_ascii=False)
