import re
import peewee as pw
import urllib
from flask import (
    render_template,
    request,
    session,
    send_from_directory,
    json,
    make_response,
    send_file
)
from ..main import main
from .models import (
    Product,
    Brand,
    ProductBrand,
    Price,
    Pagination
)

'''
import logging
logger = logging.getLogger('peewee')
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)
'''


@main.route('/', methods=['GET'])
def index():
    ordered_list = session.get('ordered_list')
    return render_template('home.html', ordered_list=ordered_list)


@main.route('/favicon.ico')
def favicon():
    return send_file('favicon.ico')


@main.route('/product/<int:id>', methods=['GET'])
def product(id):
    ordered_list = session.get('ordered_list')
    product = (Product().select(Product, Brand.brand_name)
                        .join(
                            ProductBrand, 'LEFT',
                            on=(ProductBrand.product_id == Product.id))
                        .switch(Product)
                        .join(
                            Brand, 'LEFT',
                            on=(Brand.id == ProductBrand.brand_id))
                        .where(Product.id == id)
                        .get())
    return render_template('product.html',
                           product=product,
                           ordered_list=ordered_list)


@main.route('/about', methods=['GET'])
def about():
    ordered_list = session.get('ordered_list')
    return render_template('about.html', ordered_list=ordered_list)


@main.route('/delivery', methods=['GET'])
def delivery():
    ordered_list = session.get('ordered_list')
    return render_template('delivery.html', ordered_list=ordered_list)


@main.app_template_filter('is_alpha_numeric')
def is_alpha_numeric(str=''):
    return bool(re.match('^(?=.*[0-9]$)|(?=.*[a-zA-Z])', str))


@main.route('/catalogue/', defaults={'page': 1})
@main.route('/catalogue/search/<filter>', defaults={'page': 1})
@main.route('/catalogue/page/<int:page>')
@main.route('/catalogue/search/<filter>/page/<int:page>')
def catalogue(page=1, filter=''):
    ordered_list = session.get('ordered_list')
    filter = urllib.parse.unquote(filter)
    products_per_page = 10
    if len(filter) > 0:
        decoded = urllib.parse.unquote(filter)
        '''
        Comment the following stroke because it case sensitive
        products = Product.select().where(Product.name.contains(filter))
            .paginate(
                page, products_per_page
            )
        add lowercase_search() to make search case insensitive.
        See @database.func('lowercase_search') in model.py
        '''
        products = Product().select().where(
            pw.fn.synonyms_search(Product.name, decoded)).paginate(
                page, products_per_page)
        total_products = Product.select().where(
            pw.fn.synonyms_search(Product.name, decoded)).count()
    else:
        products = Product.select().paginate(page, products_per_page)
        total_products = Product.select().count()

    # Get price date
    price_query = Price.select(pw.fn.MAX(Price.date)).scalar()
    if price_query is not None:
        price_date = price_query.strftime('%d.%m.%Y')
    else:
        price_date = '18.10.2018'

    pagination = Pagination(page, products_per_page, total_products)
    filter = Product.convert_bad_synonym_to_good_phrase(filter)

    return render_template(
        'catalogue.html',
        price_date=price_date,
        pagination=pagination,
        products=products,
        page=page,
        filter=filter,
        ordered_list=ordered_list)


@main.route('/dist/<path:path>')
def static_dist(path):
    # тут пробрасываем статику
    return send_from_directory("static/js/dist", path)


@main.route('/api/search/<phrase>')
def search_result(phrase):
    decoded = urllib.parse.unquote(phrase)
    query = Product().select().where(pw.fn.synonyms_search(
        Product.name, decoded))
    output = {}
    output['results'] = []

    for result in query:
        output['results'].append({'name': result.name})

    return json.dumps(output, ensure_ascii=False)


@main.route('/api/session', methods=["POST"])
def save_selected_product_to_session():
    if request.method == 'GET':
        return make_response('failure')
    if request.method == 'POST':
        id = request.get_json().get('id')
        command = request.get_json().get('command')
        if 'ordered_list' not in session:
            session['ordered_list'] = []
        ordered_list = session['ordered_list']
        # add to shop basket list
        if 'add' in command and id not in ordered_list:
            ordered_list.append(id)
        # remove from shop basket list
        if 'remove' in command:
            ordered_list.remove(id)

        session['ordered_list'] = ordered_list
        params = {
            'result': session['ordered_list']
        }
    return json.dumps(params)


@main.app_errorhandler(404)
def handle_404(err):
    return render_template('404.html'), 404


'''
@main.app_errorhandler(500)
def handle_500(err):
    return render_template('500.html'), 500
'''
