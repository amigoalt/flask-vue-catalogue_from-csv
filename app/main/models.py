import peewee as pw
from app.database import database
import pandas as pd
import datetime
from math import ceil
import app.helpers as hlp


@database.func('lowercase_search')
def lowercase_search(name, filter):
    return filter.lower() in name.lower()


@database.func('synonyms_search')
def synonyms_search(target, filter):
    return Product.is_in_synonyms(target, filter)


class BaseModel(pw.Model):
    class Meta:
        database = database


class Catalogue(BaseModel):
    pass


class Price(BaseModel):
    date = pw.DateField(formats=['%Y-%m-%d'], default=datetime.datetime.now)


class Owner(BaseModel):
    id = pw.PrimaryKeyField()
    name = pw.CharField()
    address = pw.CharField()
    contact = pw.CharField()

    class meta:
        table_name = 'owners'


class Product(BaseModel):
    class meta:
        table_name = 'products'

    name = pw.CharField()
    code = pw.CharField(null=True)
    price = pw.FloatField(null=True)
    photo = pw.CharField(null=True)
    content = pw.TextField(null=True)
    synonyms = {"bosch": ("bosh",
                          "b0sh",
                          "b0sch",
                          "бош",
                          "вош",
                          "б0ш",
                          "в0ш",
                          ",ji",
                          ",0i",
                          "dji"),
                "denso": ("денсо", "дэнсо", "ltycj"),
                "dens": ("денс", "дэнс", "l'yc", "ltyc"),
                "den": ("ден", "дэн", "l'y", "lty"),
                "delphi": ("делфи",
                           "дэлфи",
                           "делфай",
                           "дэлфай",
                           "дэлфу",
                           "делфу",
                           "l'kab",
                           "ltkab",
                           "ltkafq",
                           "ltkae",
                           "l'kae"),
                "delph": ("делф", "дэлф", "делфй"),
                "delp": ("делф", "дэлф", "делфй"),
                "ямз": ("zvp", "zmp", "yamz"),
                "камаз": ("rfvfp", "камас", "kamaz", "kamas"),
                "кама": ("kama", "rfvf"),
                "кам": ("kam", "rfv"),
                "ford": ("форд", "форт", "ajhn", "ajhl"),
                "man": ("ман", "мэн", "vfy"),
                "toyota": ("тойота",
                           "тоета",
                           "тоёта",
                           "njqjnf",
                           "njtnf",
                           "nj`nf"),
                "scania": ("скания", "сканья", "сканя", "crfybz", "crfymz"),
                "renault": ("рено", "ренно", "рэно", "reno", "htyj"),
                "mercedes": ("мерседес",
                             "мэрседес",
                             "мэрсэдес",
                             "мэрсэдэс",
                             "мерсэдес",
                             "мерседэс",
                             "мерс",
                             "мерсед",
                             "vthc",
                             "mersedes",
                             "mersed",
                             "mers"),
                "daw": ("даф", "дав", "dav", "daf", "lfa", "lfd"),
                "peugeot": ("пежо",
                            "пижо",
                            "pegeot",
                            "peguot",
                            "peugeo",
                            "gt;j",
                            "gb;j"),
                "тнвд": ("tnvd", "nydl")}

    @staticmethod
    def is_in_synonyms(target, filter):
        '''
        Решает проблему с русскими буквами (заглавными, строчными)
        при поиске в SQlite, не поддерживающим COLLOCATION NOCASE
        А также делает поиск по синонимам, т.е. обрабатывает опечатки
        при вводе в графе поиска.
        Функция вызывается в запросе к БД через Peewee ORM
        '''
        filter = filter.lower()
        target = target.lower()
        synonyms = Product.synonyms
        for s_key in synonyms.keys():
            if (filter in synonyms[s_key] or filter == s_key):
                filter = s_key
                break

        return filter in target

    @staticmethod
    def convert_bad_synonym_to_good_phrase(phrase=''):
        '''
        Это нужно, чтобы после неудачного ввода, напр. "мэрсэдэс",
        пользователь увидел на странице "Поиск по запросу mercedes"
        При этом в исходном csv-файле, все марки и брэнды должны быть
        приведены к правильным английским названиям: Bosch, Renault, Toyota
        '''
        synonyms = Product.synonyms
        for s_key in synonyms.keys():
            if (phrase in synonyms[s_key] or phrase == s_key):
                phrase = s_key
                break
        return phrase


class Brand(BaseModel):
    class meta:
        table_name = 'brands'

    brand_name = pw.CharField(unique=True)


class ProductBrand(BaseModel):
    class Meta:
        indexes = (
            (('brand_id', 'product_id'), True)
        ),
        table_name = 'products_brands'

    brand_id = pw.ForeignKeyField(Brand, backref='product_brand')
    product_id = pw.ForeignKeyField(Product, backref='product_brand')


class Pagination(object):
    def __init__(self, page, per_page, total_count):
        self.page = page
        self.per_page = per_page
        self.total_count = total_count

    @property
    def pages(self):
        return int(ceil(self.total_count / float(self.per_page)))

    @property
    def has_prev(self):
        return self.page > 1

    @property
    def has_next(self):
        return self.page < self.pages

    def iter_pages(self, left_edge=2, left_current=2,
                   right_current=2, right_edge=2):
        last = 0
        for num in range(1, self.pages + 1):
            if num <= left_edge or (num > self.page - left_current - 1 and
               num < self.page + right_current) or \
               num > self.pages - right_edge:
                    if last + 1 != num:
                        yield None
                    yield num
                    last = num
