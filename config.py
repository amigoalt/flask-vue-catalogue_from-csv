import os


class Config(object):
    DEBUG = False
    CSRF_ENABLED = True
    SECRET_KEY = "sp[tofirgpoeruygoprigufoiyufptoy8ef"


class ProductionConfig(Config):
    DEBUG = False
    APP_PATH = '/home/d/dizelsklad/dizelsklad.beget.tech/app'


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = False
    APP_PATH = '/home/good/python_projects/flask-vue-catalogue_from-csv/app'
